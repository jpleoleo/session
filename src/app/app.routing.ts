import { HomeComponent } from "./components/home/home.component";
import { PageAComponent } from "./components/page-a/page-a.component";
import { PageBComponent } from "./components/page-b/page-b.component";
import { Routes } from "@angular/router";

export const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'page-a', component: PageAComponent },
    { path: 'page-b', component: PageBComponent },
    { path: 'lazy', loadChildren: './components/lazy/lazy.module#LazyModule' },
    { path: '**', redirectTo: 'home' }
];