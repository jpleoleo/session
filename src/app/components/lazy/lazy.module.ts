import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyComponent } from './lazy.component';
import { Routes, RouterModule } from '@angular/router';
import { LazyAComponent } from '../lazy-a/lazy-a.component';

const routes: Routes = [
  { path: '', redirectTo: 'load-me', pathMatch: 'full' },
  { path: 'load-me', component: LazyComponent },
  { path: 'load-a', component: LazyAComponent }
];



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    LazyComponent,
    LazyAComponent
  ]
})
export class LazyModule { }
